package facci.kleber.delgado.delgadoacuartoc.menus;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import facci.kleber.delgado.delgadoacuartoc.R;

public class ActividadSensorProximidad extends AppCompatActivity implements SensorEventListener {
    LinearLayout li;
    SensorManager sensorManager;
    Sensor proximidad;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_proximidad);
        li=(LinearLayout)findViewById(R.id.li);
        textView=(TextView)findViewById(R.id.txt);
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        proximidad=sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorManager.registerListener(this,proximidad,sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
    String text = String.valueOf(event.values[0]);
    textView.setText(text);
    float valor = Float.parseFloat(text);
    if (valor==0) {
        li.setBackgroundColor(Color.GREEN);
    }
    else{
        li.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
