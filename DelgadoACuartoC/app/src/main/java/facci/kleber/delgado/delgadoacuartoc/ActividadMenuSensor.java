package facci.kleber.delgado.delgadoacuartoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorAcelerometro;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorLuz;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorProximidad;

public class ActividadMenuSensor extends AppCompatActivity  {
    Button buttonAceleromentro, buttonProximidad, buttonLuz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_menu_sensor);
        buttonAceleromentro = (Button) findViewById(R.id.btnAcelerometro);
        buttonProximidad = (Button) findViewById(R.id.btnProximidad);
        buttonLuz = (Button) findViewById(R.id.btnLuz);

        buttonAceleromentro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadMenuSensor.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });
        buttonProximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadMenuSensor.this, ActividadSensorProximidad.class);
                startActivity(intent);
            }
        });
        buttonLuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadMenuSensor.this, ActividadSensorLuz.class);
                startActivity(intent);
            }
        });
    }
}
