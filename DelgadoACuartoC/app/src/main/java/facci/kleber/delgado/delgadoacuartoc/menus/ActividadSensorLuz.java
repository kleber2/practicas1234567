package facci.kleber.delgado.delgadoacuartoc.menus;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import facci.kleber.delgado.delgadoacuartoc.R;

public class ActividadSensorLuz extends AppCompatActivity implements SensorEventListener {
    SensorManager sensorManager;
    Sensor luz;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_luz);
        textView=(TextView)findViewById(R.id.txtluz);
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        luz=sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,luz,sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
      if (event.sensor.getType()==Sensor.TYPE_LIGHT){
          textView.setText(""+event.values[0]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
